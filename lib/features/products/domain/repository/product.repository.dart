import 'package:login_app/features/products/domain/domain.module.dart';

abstract class ProductRepository {
  Future<List<ProductEntity>> getProductsByPage({int limit = 10, int offset = 0});
  Future<ProductEntity> getProductById(String id);
  Future<List<ProductEntity>> searchProductByTerm(String term);
  Future<ProductEntity> addAndUpdateProduct(Map<String, dynamic> productLike);
}
