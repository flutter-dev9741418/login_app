import 'package:flutter/material.dart';
import 'package:login_app/features/products/domain/domain.module.dart';

class ProductCard extends StatelessWidget {
  final ProductEntity product;

  const ProductCard({
    super.key,
    required this.product,
  });

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _ImageViewer(
          images: product.images,
        ),
        Text(
          product.title,
          style: textTheme.titleMedium,
          textAlign: TextAlign.center,
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}

class _ImageViewer extends StatelessWidget {
  final List<String> images;

  const _ImageViewer({
    required this.images,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: images.isEmpty
          ? Image.asset(
              "assets/img/no-image.jpg",
              fit: BoxFit.cover,
              height: 250,
            )
          : FadeInImage(
              height: 250,
              fadeOutDuration: const Duration(milliseconds: 100),
              fadeInDuration: const Duration(milliseconds: 200),
              placeholder: const AssetImage("assets/img/loaders/bottle-loader.gif"),
              image: NetworkImage(images.first),
            ),
    );
  }
}
