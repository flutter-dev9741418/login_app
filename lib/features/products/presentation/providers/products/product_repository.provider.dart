import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:login_app/features/auth/presentation/providers/provider.module.dart';
import 'package:login_app/features/products/infrastructure/infrastructure.module.dart';
import 'package:login_app/features/products/domain/domain.module.dart';

final productRepositoryProvider = Provider<ProductRepository>((ref) {
  final accessToken = ref.watch(authProvider).user?.token ?? "";

  final productRepository = ProductImplRepository(
    ProductImplDatasource(
      accessToken: accessToken,
    ),
  );

  return productRepository;
});
