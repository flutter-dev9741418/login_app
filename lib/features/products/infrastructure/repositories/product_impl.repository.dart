import 'package:login_app/features/products/domain/domain.module.dart';
import 'package:login_app/features/products/infrastructure/infrastructure.module.dart';

class ProductImplRepository extends ProductRepository {
  final ProductDatasource datasource;

  ProductImplRepository(
    ProductDatasource? datasource,
  ) : datasource = datasource ?? ProductImplDatasource(accessToken: "");

  @override
  Future<ProductEntity> addAndUpdateProduct(Map<String, dynamic> productLike) {
    return datasource.addAndUpdateProduct(productLike);
  }

  @override
  Future<ProductEntity> getProductById(String id) {
    return datasource.getProductById(id);
  }

  @override
  Future<List<ProductEntity>> getProductsByPage({int limit = 10, int offset = 0}) {
    return datasource.getProductsByPage(limit: limit, offset: offset);
  }

  @override
  Future<List<ProductEntity>> searchProductByTerm(String term) {
    return datasource.searchProductByTerm(term);
  }
}
