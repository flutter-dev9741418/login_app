import 'package:login_app/config/config.module.dart';
import 'package:login_app/features/auth/infrastructure/infrastructure.module.dart';
import 'package:login_app/features/products/domain/domain.module.dart';

class ProductMapper {
  static ProductEntity productJsonToEntity(Map<String, dynamic> json) {
    return ProductEntity(
      id: json["id"],
      title: json["title"],
      price: double.parse(json["price"]),
      description: json["description"],
      slug: json["slug"],
      stock: json["stock"],
      sizes: List<String>.from(json["sizes"].map((x) => x)),
      gender: json["gender"],
      tags: List<String>.from(json["tags"].map((x) => x)),
      images: List<String>.from(
        json["images"].map(
          (x) => x.startsWith("http") ? x : '${Environment.apiUrl}/files/product/$x',
        ),
      ),
      user: UserMapper.userJsonToEntity(json["user"]),
    );
  }
}
