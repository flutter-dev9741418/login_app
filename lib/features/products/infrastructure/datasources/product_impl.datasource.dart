import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:login_app/config/config.module.dart';
import 'package:login_app/features/products/domain/domain.module.dart';
import 'package:login_app/features/products/infrastructure/infrastructure.module.dart';
import 'package:login_app/features/shared/shared.dart';

class ProductImplDatasource extends ProductDatasource {
  late final Dio dio;
  final String accessToken;

  ProductImplDatasource({
    required this.accessToken,
  }) : dio = Dio(
          BaseOptions(
            baseUrl: Environment.apiUrl,
            headers: {
              "Authorization": "Bearer $accessToken",
            },
          ),
        );

  @override
  Future<ProductEntity> addAndUpdateProduct(Map<String, dynamic> productLike) {
    throw UnimplementedError();
  }

  @override
  Future<ProductEntity> getProductById(String id) {
    throw UnimplementedError();
  }

  @override
  Future<List<ProductEntity>> getProductsByPage({int limit = 10, int offset = 0}) async {
    try {
      final res = await dio.get<List>("/products?limit=$limit&offset=$offset");
      final List<ProductEntity> products = [];

      for (final p in res.data ?? []) {
        products.add(ProductMapper.productJsonToEntity(p));
      }

      return products;
    } on DioException catch (e) {
      debugPrint("$e");
      if (e.response?.statusCode == 401) {
        throw CustomError(message: "Token invalido.", statusCode: e.response!.statusCode!);
      } else if (e.type == DioExceptionType.connectionTimeout) {
        throw CustomError(
            message: DioExceptionType.connectionTimeout.toString(), statusCode: e.response!.statusCode!);
      } else {
        throw CustomError(
            message: "Error inesperado, comuniquese con el administrador del sistema.", statusCode: 500);
      }
    } catch (e) {
      debugPrint("$e");
      throw CustomError(
          message: "Error inesperado, comuniquese con el administrador del sistema.", statusCode: 500);
    }
  }

  @override
  Future<List<ProductEntity>> searchProductByTerm(String term) {
    throw UnimplementedError();
  }
}
