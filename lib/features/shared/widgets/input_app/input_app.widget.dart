import 'package:flutter/material.dart';

class InputApp extends StatelessWidget {
  final String? label;
  final IconData? iconLeft;
  final IconData? iconRight;
  final String placeholder;
  final TextInputType type;
  final bool typePass;
  final bool onlyNumber;
  final String? errorMessage;
  final Function(String)? onChanged;
  final Function(String)? onFieldSubmitted;
  final String? Function(String?)? validator;
  final int? maxLength;

  const InputApp({
    super.key,
    this.type = TextInputType.text,
    this.label,
    this.placeholder = "",
    this.iconLeft,
    this.iconRight,
    this.errorMessage,
    this.onChanged,
    this.onFieldSubmitted,
    this.validator,
    this.typePass = false,
    this.onlyNumber = false,
    this.maxLength,
  });

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;

    final border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
    );

    return TextFormField(
      maxLength: maxLength,
      keyboardType: typePass
          ? onlyNumber
              ? TextInputType.number
              : TextInputType.text
          : type,
      onChanged: onChanged,
      onFieldSubmitted: onFieldSubmitted,
      validator: validator,
      obscureText: typePass,
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border.copyWith(
          borderSide: BorderSide(
            color: colors.primary,
          ),
        ),
        errorBorder: border.copyWith(
          borderSide: BorderSide(
            color: colors.error,
          ),
        ),
        focusedErrorBorder: border.copyWith(
          borderSide: BorderSide(
            color: colors.error,
          ),
        ),
        errorText: errorMessage,
        isDense: true,
        label: label != null
            ? label!.isNotEmpty
                ? Text(label!)
                : null
            : null,
        hintText: placeholder,
        focusColor: colors.primary,
        prefixIcon: (iconLeft != null)
            ? Icon(
                iconLeft,
                color: colors.primary,
              )
            : null,
        suffixIcon: (iconRight != null)
            ? Icon(
                iconRight,
                color: colors.primary,
              )
            : null,
      ),
    );
  }
}
