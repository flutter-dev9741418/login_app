import 'package:shared_preferences/shared_preferences.dart';
import 'localstorage.service.dart';

class LocalStorageImplService extends LocalStorageService {
  Future<SharedPreferences> getSharedPrefs() async {
    return await SharedPreferences.getInstance();
  }

  @override
  Future<T?> getItem<T>(String key) async {
    final prefs = await getSharedPrefs();
    switch (T) {
      case int:
        return prefs.getInt(key) as T?;
      case String:
        return prefs.getString(key) as T?;
      case bool:
        return prefs.getBool(key) as T?;
      case double:
        return prefs.getDouble(key) as T?;
      case List<String>:
        return prefs.getStringList(key) as T?;
      default:
        throw UnimplementedError('Get not implement for type ${T.runtimeType}');
    }
  }

  @override
  Future<bool> removeItem(String key) async {
    final prefs = await getSharedPrefs();
    return prefs.remove(key);
  }

  @override
  Future<void> setItem<T>(String key, T value) async {
    final prefs = await getSharedPrefs();
    switch (T) {
      case int:
        prefs.setInt(key, value as int);
        break;
      case String:
        prefs.setString(key, value as String);
        break;
      case bool:
        prefs.setBool(key, value as bool);
        break;
      case double:
        prefs.setDouble(key, value as double);
        break;
      case List<String>:
        prefs.setStringList(key, value as List<String>);
        break;
      default:
        throw UnimplementedError('Set not implement for type ${T.runtimeType}');
    }
  }
}
