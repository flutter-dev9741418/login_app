import 'package:login_app/features/auth/domain/domain.module.dart';
import '../infrastructure.module.dart';

class AuthImplRepository extends AuthRepository {
  final AuthDatasource _datasource;

  AuthImplRepository({
    AuthDatasource? datasource,
  }) : _datasource = datasource ?? AuthImplDatasource();

  @override
  Future<UserEntity> isAuthenticated(String token) {
    return _datasource.isAuthenticated(token);
  }

  @override
  Future<UserEntity> login(String email, String password) {
    return _datasource.login(email, password);
  }

  @override
  Future<UserEntity> register(String email, String password, String fullName) {
    return _datasource.register(email, password, fullName);
  }
}
