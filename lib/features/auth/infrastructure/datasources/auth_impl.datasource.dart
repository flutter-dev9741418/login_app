import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:login_app/config/config.module.dart';
import 'package:login_app/features/auth/domain/domain.module.dart';
import 'package:login_app/features/auth/infrastructure/infrastructure.module.dart';
import 'package:login_app/features/shared/shared.dart';

class AuthImplDatasource extends AuthDatasource {
  final dio = Dio(
    BaseOptions(
      baseUrl: Environment.apiUrl,
    ),
  );

  @override
  Future<UserEntity> isAuthenticated(String token) async {
    try {
      final res = await dio.post(
        "/auth/check-status",
        options: Options(
          headers: {"Authorization": "Bearer $token"},
        ),
      );

      final user = UserMapper.userJsonToEntity(res.data);

      return user;
    } on DioException catch (e) {
      debugPrint("$e");
      if (e.response?.statusCode == 401) {
        throw CustomError(message: "Sesión vencida.", statusCode: e.response!.statusCode!);
      } else if (e.type == DioExceptionType.connectionTimeout) {
        throw CustomError(
            message: DioExceptionType.connectionTimeout.toString(), statusCode: e.response!.statusCode!);
      } else {
        throw CustomError(message: "Error inesperado al refrescar la sesión", statusCode: 500);
      }
    } catch (e) {
      debugPrint("$e");
      throw CustomError(message: "Error inesperado al refrescar la sesión", statusCode: 500);
    }
  }

  @override
  Future<UserEntity> login(String email, String password) async {
    try {
      final res = await dio.post("/auth/login", data: {
        "email": email,
        "password": password,
      });
      final user = UserMapper.userJsonToEntity(res.data);
      return user;
    } on DioException catch (e) {
      debugPrint("$e");
      if (e.response?.statusCode == 401) {
        throw CustomError(message: "Usuario o contraseña incorrectos.", statusCode: e.response!.statusCode!);
      } else if (e.type == DioExceptionType.connectionTimeout) {
        throw CustomError(
            message: DioExceptionType.connectionTimeout.toString(), statusCode: e.response!.statusCode!);
      } else {
        throw CustomError(message: "Error inesperado al iniciar sesión", statusCode: 500);
      }
    } catch (e) {
      debugPrint("$e");
      throw CustomError(message: "Error inesperado al iniciar sesión", statusCode: 500);
    }
  }

  @override
  Future<UserEntity> register(String email, String password, String fullName) {
    throw UnimplementedError();
  }
}
