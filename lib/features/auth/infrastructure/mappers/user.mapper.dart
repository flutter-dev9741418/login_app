import 'package:login_app/features/auth/domain/domain.module.dart';

class UserMapper {
  static UserEntity userJsonToEntity(Map<String, dynamic> json) {
    return UserEntity(
      id: json["id"],
      email: json["email"],
      fullName: json["fullName"],
      roles: List<String>.from(json["roles"].map((role) => role)),
      token: json["token"] ?? '',
    );
  }
}
