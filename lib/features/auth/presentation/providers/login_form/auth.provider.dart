import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:login_app/features/auth/domain/domain.module.dart';
import 'package:login_app/features/auth/infrastructure/infrastructure.module.dart';
import 'package:login_app/features/shared/infrastructure/services/localstorage.service.dart';
import 'package:login_app/features/shared/infrastructure/services/localstorage_impl.service.dart';
import 'package:login_app/features/shared/shared.dart';

final authProvider = StateNotifierProvider<AuthNotifier, AuthState>((ref) {
  final authRepository = AuthImplRepository();
  final localStorage = LocalStorageImplService();

  return AuthNotifier(
    authRepository: authRepository,
    localStorage: localStorage,
  );
});

class AuthNotifier extends StateNotifier<AuthState> {
  final AuthRepository authRepository;
  final LocalStorageService localStorage;

  AuthNotifier({
    required this.authRepository,
    required this.localStorage,
  }) : super(AuthState()) {
    isAuthenticated();
  }

  Future<void> loginUser(String email, String password) async {
    await Future.delayed(const Duration(milliseconds: 500));

    try {
      final user = await authRepository.login(email, password);
      _setAuth(user);
    } on CustomError catch (e) {
      logout(e.message);
    }
  }

  void registerUser(String email, String password, String fullName) async {}

  void isAuthenticated() async {
    final token = await localStorage.getItem<String>("token");

    if (token == null) return logout();

    try {
      final user = await authRepository.isAuthenticated(token);
      _setAuth(user);
    } on CustomError catch (e) {
      logout(e.message);
    }
  }

  void _setAuth(UserEntity user) async {
    await localStorage.setItem("token", user.token);

    state = state.copyWith(
      user: user,
      authStatusType: AuthStatusType.authenticated,
      errorMessage: "",
    );
  }

  Future<void> logout([String? errorMessage]) async {
    await localStorage.removeItem("token");

    state = state.copyWith(
      user: null,
      authStatusType: AuthStatusType.noAuthenticated,
      errorMessage: errorMessage,
    );

    // appRouter.go("/login");
  }
}

enum AuthStatusType { checking, authenticated, noAuthenticated }

class AuthState {
  final AuthStatusType authStatusType;
  final UserEntity? user;
  final String errorMessage;

  AuthState({
    this.authStatusType = AuthStatusType.checking,
    this.user,
    this.errorMessage = "",
  });

  AuthState copyWith({
    AuthStatusType? authStatusType,
    UserEntity? user,
    String? errorMessage,
  }) {
    return AuthState(
      authStatusType: authStatusType ?? this.authStatusType,
      user: user ?? this.user,
      errorMessage: errorMessage ?? this.errorMessage,
    );
  }
}
