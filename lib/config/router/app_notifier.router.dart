import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../features/auth/presentation/providers/login_form/auth.provider.dart';

final goRouterNotifierProvider = Provider((ref) {
  final authNotifier = ref.read(authProvider.notifier);
  return GoRouterNotifier(authNotifier);
});

class GoRouterNotifier extends ChangeNotifier {
  final AuthNotifier _authNotifier;

  AuthStatusType _authStatusType = AuthStatusType.checking;

  GoRouterNotifier(this._authNotifier) {
    _authNotifier.addListener((state) {
      authStatusType = state.authStatusType;
    });
  }

  AuthStatusType get authStatusType => _authStatusType;

  set authStatusType(AuthStatusType value) {
    _authStatusType = value;
    notifyListeners();
  }
}
