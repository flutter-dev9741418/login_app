import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:login_app/config/router/app_notifier.router.dart';

import '../../features/auth/auth.dart';
import '../../features/auth/presentation/providers/login_form/auth.provider.dart';
import '../../features/products/products.dart';

final goRouterProvider = Provider<GoRouter>((ref) {
  final goRouterNotifier = ref.read(goRouterNotifierProvider);

  return GoRouter(
    initialLocation: '/splash',
    refreshListenable: goRouterNotifier,
    routes: [
      GoRoute(
        path: '/splash',
        builder: (context, state) => const CheckAuthScreen(),
      ),
      GoRoute(
        path: '/login',
        builder: (context, state) => const LoginScreen(),
      ),
      GoRoute(
        path: '/register',
        builder: (context, state) => const RegisterScreen(),
      ),
      GoRoute(
        path: '/',
        builder: (context, state) => const ProductsScreen(),
      ),
    ],
    redirect: (context, state) {
      final isGoingTo = state.fullPath;
      final authStatus = goRouterNotifier.authStatusType;

      if (isGoingTo == "/splash" && authStatus == AuthStatusType.checking) return null;

      if (authStatus == AuthStatusType.noAuthenticated) {
        if (isGoingTo == "/login" || isGoingTo == "/register") return null;

        return "/login";
      }

      if (authStatus == AuthStatusType.authenticated) {
        if (isGoingTo == "/login" || isGoingTo == "/register") return "/";
      }

      return null;
    },
  );
});
